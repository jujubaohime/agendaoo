package application;

import core.Agenda;
import core.Janela;
import core.Pessoa;

import javax.swing.*;

import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

import java.util.ArrayList;
import java.util.List;

public class Program {
    public static void main(String[] args) {
        List<Pessoa> pessoas = new ArrayList<>();
        Agenda agenda = new Agenda(pessoas);
        agenda.popularAgenda();
        Janela janela = new Janela();
        janela.setVisible(true);
        janela.setSize(500, 500);
        janela.setDefaultCloseOperation(EXIT_ON_CLOSE);
        
    }
}
