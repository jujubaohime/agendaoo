package core;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Agenda{
    
    private List<Pessoa> pessoas;


    public Agenda(List<Pessoa> pessoas) {
        this.pessoas = pessoas;
    }

    public List<Pessoa> getPessoas() {
        return pessoas;
    }

    public Pessoa getPessoa(String name) {
        Pessoa f = new Pessoa("", "", "");
        for (Pessoa p : pessoas) {
            if (p.toString().compareTo(name) == 0){
                return p;
            }
        }
        return f;
    }

    @Override
    public String toString() {
        return "Agenda{" +
                "pessoas=" + pessoas +
                '}';
    }

    public void popularAgenda(){
        String name="", details = "", phone = "";
        for(int i = 0; i<10; i++){
            name = name + "a";
            details = details + "b";
            phone = phone + "i";
            addPerson(name, phone, details);
        }
    }

    public void addPerson(String name, String phone, String details){
        Pessoa pessoa = new Pessoa(name, phone, details);
        pessoas.add(pessoa);
    }

    public void removePerson(String name){
        boolean entrou = false;
        for (Pessoa pessoa : pessoas){
            if (name.equals(pessoa.getName())){
                pessoas.remove(pessoa);
                entrou = true;
                break;
            }
        }
        if(!entrou) System.out.println("Não há tal elemento na agenda");
    }

    public void editPerson(Pessoa pessoa,String name, String phone, String details) {
        pessoa.setDetails(details);
        pessoa.setName(name);
        pessoa.setPhone(phone);
    }

}
