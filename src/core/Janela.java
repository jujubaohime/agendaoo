package core;

import java.util.ArrayList;
import java.util.List;

import javax.swing.*;

import com.sun.java.swing.action.NewAction;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Janela
 */
public class Janela extends JFrame {

    JTextField nome_field;
    JTextField telefone_field;
    JTextArea detalhes_field;
    Agenda agenda;
    DefaultListModel list_model = new DefaultListModel<String>();

    public Janela() {
        super("Agenda");
        this.agenda = new Agenda(new ArrayList<Pessoa>());

        cria_base();
    }

    private void cria_base() {
        setLayout(new BorderLayout());

        JPanel panel_lista = new JPanel();
        panel_lista.setLayout(new FlowLayout());

        JList lista = new JList(list_model);
        lista.setModel(list_model);
        lista.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        lista.getSelectionModel().addListSelectionListener(e -> {
            if (!lista.isSelectionEmpty()){
                String s = lista.getSelectedValue().toString();
                Pessoa p = agenda.getPessoa(s);
                if (p == null) System.out.println("BUG");
                nome_field.setText(p.getName());
                telefone_field.setText(p.getPhone());
                detalhes_field.setText(p.getDetails());
                lista.clearSelection();
            }
        
        });


        JScrollPane scroll = new JScrollPane(lista);
        scroll.setPreferredSize(new Dimension(250, 400));
        panel_lista.add(scroll);

        JPanel panel_cadastro = new JPanel();
        panel_cadastro.setLayout(new BorderLayout());
        panel_cadastro.setPreferredSize(new Dimension(250, 400));

        JPanel panel_form = new JPanel();
        panel_form.setLayout(new FlowLayout());
        panel_form.setPreferredSize(new Dimension(250, 300));

        JPanel panel_nome = new JPanel();
        JLabel nome = new JLabel("Nome");
        nome_field = new JTextField(10);
        panel_nome.add(nome);
        panel_nome.add(nome_field);

        JPanel panel_telefone = new JPanel();
        JLabel telefone = new JLabel("Telefone");
        telefone_field = new JTextField(10);
        panel_telefone.add(telefone);
        panel_telefone.add(telefone_field);

        JPanel panel_detalhes = new JPanel();
        JLabel detalhes = new JLabel("Detalhes");
        detalhes_field = new JTextArea(5, 10);
        panel_detalhes.add(detalhes);
        panel_detalhes.add(detalhes_field);

        JPanel panel_botoes = new JPanel();
        panel_botoes.setLayout(new FlowLayout());
        panel_botoes.setPreferredSize(new Dimension(250, 100));

        panel_form.add(panel_nome);
        panel_form.add(panel_telefone);
        panel_form.add(panel_detalhes);

        AddAction addAction = new AddAction();
        JButton adicionar = new JButton("adicionar");
        adicionar.addActionListener(addAction);

        RemoveAction removeAction = new RemoveAction();
        JButton remover = new JButton("remover");
        remover.addActionListener(removeAction);

        EditAction editAction = new EditAction();
        JButton atualizar = new JButton("atualizar");
        atualizar.addActionListener(editAction);

        panel_botoes.add(adicionar);
        panel_botoes.add(remover);
        panel_botoes.add(atualizar);

        panel_cadastro.add(panel_form, BorderLayout.NORTH);
        panel_cadastro.add(panel_botoes, BorderLayout.SOUTH);

        add(panel_lista, BorderLayout.WEST);
        add(panel_cadastro, BorderLayout.EAST);

    }

    private class AddAction implements ActionListener {
    
        @Override
        public void actionPerformed(ActionEvent e) {
            if(!list_model.contains(nome_field.getText())){
                agenda.addPerson(nome_field.getText(), telefone_field.getText(), detalhes_field.getText());
                list_model.addElement(nome_field.getText());
            }
            
        }
    
        
    }
    
    private class RemoveAction implements ActionListener {
        
        @Override
        public void actionPerformed(ActionEvent e) {
            if(list_model.contains(nome_field.getText())){
                agenda.removePerson(nome_field.getText());
                list_model.removeElement(nome_field.getText());
            }
        }
    
        
    }
    
    private class EditAction implements ActionListener {
    
        @Override
        public void actionPerformed(ActionEvent e) {
            if(list_model.contains(nome_field.getText())){
                agenda.editPerson(agenda.getPessoa(nome_field.getText()), nome_field.getText(), telefone_field.getText(), detalhes_field.getText());
            }
        }
    
        
    }
}
