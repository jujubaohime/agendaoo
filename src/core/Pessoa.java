package core;

public class Pessoa {
    private String name;
    private String phone;
    private String details;

    public Pessoa(String name, String phone, String details) {
        this.name = name;
        this.phone = phone;
        this.details = details;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
